package com.habitrack.app.services;


import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TimePicker;

import com.habitrack.app.R;
import com.habitrack.app.data.DaysOfWeek;
import com.habitrack.app.data.HabitEntity;
import com.habitrack.app.data.HabitToWeekdaysEntity;
import com.habitrack.app.data.HabitType;
import com.habitrack.app.data.ReminderEntity;
import com.habitrack.app.handlers.WeekDayCheckboxHandler;
import com.j256.ormlite.dao.ForeignCollection;

public class EditOrNewHabitInitialisationHelper {

    public void initReminderCheckboxes(final View dialogView, HabitEntity habitEntity) {
        final CheckBox reminderCheckbox = (CheckBox) dialogView.findViewById(R.id.reminder_checkbox);
        final CheckBox specialReminderCheckbox = (CheckBox) dialogView.findViewById(R.id.special_reminder_checkbox);

        if (habitEntity != null) {
            reminderCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    reminderCheckboxInitialization(dialogView, reminderCheckbox, specialReminderCheckbox);
                }
            });
            specialReminderCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    specialReminderCheckboxInitialization(dialogView, specialReminderCheckbox);
                }
            });

            reminderCheckbox.setChecked(habitEntity.isReminder());
            specialReminderCheckbox.setChecked(habitEntity.isSpecialReminder());
            reminderCheckbox.setOnCheckedChangeListener(null);
            specialReminderCheckbox.setOnCheckedChangeListener(null);
        }

        reminderCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reminderCheckboxInitialization(dialogView, reminderCheckbox, specialReminderCheckbox);

            }
        });
        specialReminderCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                specialReminderCheckboxInitialization(dialogView, specialReminderCheckbox);
            }
        });
    }

    private void specialReminderCheckboxInitialization(View dialogView, CheckBox specialReminderCheckbox) {
        View specialReminderView = dialogView.findViewById(R.id.tab_host);
        TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.start_reminder_time_picker);
        if (specialReminderCheckbox.isChecked()) {
            timePicker.setVisibility(View.GONE);
            specialReminderView.setVisibility(View.VISIBLE);
        } else {
            timePicker.setVisibility(View.VISIBLE);
            specialReminderView.setVisibility(View.GONE);
        }
    }

    private void reminderCheckboxInitialization(View dialogView, CheckBox reminderCheckbox, CheckBox specialReminderCheckbox) {
        TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.start_reminder_time_picker);
        View specialReminderView = dialogView.findViewById(R.id.tab_host);

        if (reminderCheckbox.isChecked()) {
            specialReminderCheckbox.setVisibility(View.VISIBLE);
            if (specialReminderCheckbox.isChecked()) {
                timePicker.setVisibility(View.GONE);
                specialReminderView.setVisibility(View.VISIBLE);
            } else {
                timePicker.setVisibility(View.VISIBLE);
                specialReminderView.setVisibility(View.GONE);
            }
        } else {
            specialReminderCheckbox.setVisibility(View.GONE);
            timePicker.setVisibility(View.GONE);
            specialReminderView.setVisibility(View.GONE);
        }
    }


    public void initTabs(View dialogView) {
        TabHost tabhost = (TabHost) dialogView.findViewById(R.id.tab_host);
        tabhost.setup();
        tabInitialization("tag1",R.id.mon_time_picker,"Mon", tabhost);
        tabInitialization("tag2",R.id.tue_time_picker,"Tue",tabhost);
        tabInitialization("tag3",R.id.wed_time_picker,"Wed",tabhost);
        tabInitialization("tag4",R.id.thu_time_picker,"Thu",tabhost);
        tabInitialization("tag5",R.id.fri_time_picker,"Fri",tabhost);
        tabInitialization("tag6",R.id.sat_time_picker,"Sat",tabhost);
        tabInitialization("tag7",R.id.sun_time_picker,"Sun",tabhost);
    }

    private void tabInitialization(String tag, int id, String day, TabHost tabhost) {
        TabHost.TabSpec ts = tabhost.newTabSpec(tag);
        ts.setContent(id);
        ts.setIndicator(day);
        tabhost.addTab(ts);
    }

    public void initDaysCheckboxes(View dialogView, HabitEntity habitEntity) {
        TabHost tabhost = (TabHost) dialogView.findViewById(R.id.tab_host);

        CheckBox dayCheckbox = (CheckBox) dialogView.findViewById(R.id.mon_checkbox);
        prepareDayCheckbox(dayCheckbox,0,tabhost,habitEntity,DaysOfWeek.MON);

        dayCheckbox = (CheckBox) dialogView.findViewById(R.id.tue_checkbox);
        prepareDayCheckbox(dayCheckbox,1,tabhost,habitEntity,DaysOfWeek.TUE);

        dayCheckbox = (CheckBox) dialogView.findViewById(R.id.wed_checkbox);
        prepareDayCheckbox(dayCheckbox,2,tabhost,habitEntity,DaysOfWeek.WED);

        dayCheckbox = (CheckBox) dialogView.findViewById(R.id.thu_checkbox);
        prepareDayCheckbox(dayCheckbox,3,tabhost,habitEntity,DaysOfWeek.THU);

        dayCheckbox = (CheckBox) dialogView.findViewById(R.id.fri_checkbox);
        prepareDayCheckbox(dayCheckbox,4,tabhost,habitEntity,DaysOfWeek.FRI);

        dayCheckbox = (CheckBox) dialogView.findViewById(R.id.sat_checkbox);
        prepareDayCheckbox(dayCheckbox,5,tabhost,habitEntity,DaysOfWeek.SAT);

        dayCheckbox = (CheckBox) dialogView.findViewById(R.id.sun_checkbox);
        prepareDayCheckbox(dayCheckbox,6,tabhost,habitEntity,DaysOfWeek.SUN);

    }

    private void prepareDayCheckbox(CheckBox dayCheckbox, int tabNumber, TabHost tabhost, HabitEntity habitEntity, DaysOfWeek day) {
        if (habitEntity != null) {
            dayCheckbox.setOnCheckedChangeListener(new WeekDayCheckboxHandler(tabNumber, dayCheckbox, tabhost));
            dayCheckbox.setChecked(false);
            ForeignCollection<HabitToWeekdaysEntity> weekdays = habitEntity.getWeekdays();
            for (HabitToWeekdaysEntity weekday : weekdays) {
                if (weekday.getDay() == day) {
                    dayCheckbox.setChecked(true);
                }
            }
            dayCheckbox.setOnCheckedChangeListener(null);
        }
        dayCheckbox.setOnClickListener(new WeekDayCheckboxHandler(tabNumber, dayCheckbox, tabhost));
    }

    public void initReminders(View dialogView, HabitEntity habitEntity) {
        if (habitEntity != null) {
            TimePicker reminder = (TimePicker) dialogView.findViewById(R.id.start_reminder_time_picker);
            prepareReminder(reminder,habitEntity,null);

            reminder = (TimePicker) dialogView.findViewById(R.id.mon_time_picker);
            prepareReminder(reminder,habitEntity,DaysOfWeek.MON);

            reminder = (TimePicker) dialogView.findViewById(R.id.tue_time_picker);
            prepareReminder(reminder,habitEntity,DaysOfWeek.TUE);

            reminder = (TimePicker) dialogView.findViewById(R.id.wed_time_picker);
            prepareReminder(reminder,habitEntity,DaysOfWeek.WED);

            reminder = (TimePicker) dialogView.findViewById(R.id.thu_time_picker);
            prepareReminder(reminder,habitEntity,DaysOfWeek.THU);

            reminder = (TimePicker) dialogView.findViewById(R.id.fri_time_picker);
            prepareReminder(reminder,habitEntity,DaysOfWeek.FRI);

            reminder = (TimePicker) dialogView.findViewById(R.id.sat_time_picker);
            prepareReminder(reminder,habitEntity,DaysOfWeek.SAT);

            reminder = (TimePicker) dialogView.findViewById(R.id.sun_time_picker);
            prepareReminder(reminder,habitEntity,DaysOfWeek.SUN);

        }
    }

    public void prepareReminder(TimePicker reminder, HabitEntity habitEntity, DaysOfWeek day) {
        ForeignCollection<ReminderEntity> reminders = habitEntity.getReminders();
        for (ReminderEntity reminderEntity : reminders) {
            if (reminderEntity.getDay() == day) {
                reminder.setCurrentHour((int)(reminderEntity.getStartDate() / (1000 * 60 * 60)) % 24);
                reminder.setCurrentMinute((int) ((reminderEntity.getStartDate() / (1000 * 60)) % 60));
                return;
            }
        }

    }

    public void initHabitName(View dialogView, String habitName) {
        TextView name = (TextView) dialogView.findViewById(R.id.habit_name);
        name.setText(habitName);
    }

    public void initHabitType(View dialogView, HabitEntity habitEntity) {
        RadioGroup habitType = (RadioGroup) dialogView.findViewById(R.id.habit_type);
        HabitType type = habitEntity.getType();
        int checked = R.id.positive_type;
        switch (type) {
            case NEGATIVE: {
                checked = R.id.negative_type;
                break;
            }
            case NEUTRAL: {
                checked = R.id.negative_type;
                break;
            }
        }
        habitType.check(checked);
    }
}

package com.habitrack.app.services;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.habitrack.app.R;
import com.habitrack.app.data.HabitEntity;

import java.util.List;


public class HabitAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    List<HabitEntity> objects;

    public HabitAdapter(Context context, List<HabitEntity> products) {
        ctx = context;
        objects = products;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.habit_item_layout, parent, false);
        }

        HabitEntity habit = getHabit(position);
        TextView habitName = (TextView) view.findViewById(R.id.habitName);

        habitName.setText(habit.getName());

        habitName.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });

        return view;
    }


    HabitEntity getHabit(int position) {
        return ((HabitEntity) getItem(position));
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}

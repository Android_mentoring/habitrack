package com.habitrack.app.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class ReminderEntity {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign = true, columnDefinition = "integer references habit(id) on delete cascade")
    private HabitEntity habit;

    @DatabaseField
    private DaysOfWeek day;

    @DatabaseField
    private Long startDate;

    @DatabaseField
    private Long endDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HabitEntity getHabit() {
        return habit;
    }

    public void setHabit(HabitEntity habit) {
        this.habit = habit;
    }

    public DaysOfWeek getDay() {
        return day;
    }

    public void setDay(DaysOfWeek day) {
        this.day = day;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public ReminderEntity() {
    }
}

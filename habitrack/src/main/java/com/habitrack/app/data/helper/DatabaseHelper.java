package com.habitrack.app.data.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.habitrack.app.R;
import com.habitrack.app.data.HabitEntity;
import com.habitrack.app.data.HabitToWeekdaysEntity;
import com.habitrack.app.data.JournalEntity;
import com.habitrack.app.data.ReminderEntity;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "habiTrack.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<HabitEntity, Integer> habitDao = null;
    private Dao<ReminderEntity, Integer> reminderDao = null;
    private Dao<JournalEntity, Integer> journalDao = null;
    private Dao<HabitToWeekdaysEntity, Integer> habitToWeekdayDao = null;
    private RuntimeExceptionDao<HabitEntity, Integer> habitRuntimeDao = null;
    private RuntimeExceptionDao<ReminderEntity, Integer> reminderRuntimeDao = null;
    private RuntimeExceptionDao<JournalEntity, Integer> journalRuntimeDao = null;
    private RuntimeExceptionDao<HabitToWeekdaysEntity, Integer> habitToWeekdayRuntimeDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, HabitEntity.class);
            TableUtils.createTable(connectionSource, ReminderEntity.class);
            TableUtils.createTable(connectionSource, HabitToWeekdaysEntity.class);
            TableUtils.createTable(connectionSource, JournalEntity.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, HabitEntity.class, true);
            TableUtils.dropTable(connectionSource, ReminderEntity.class, true);
            TableUtils.dropTable(connectionSource, HabitToWeekdaysEntity.class, true);
            TableUtils.dropTable(connectionSource, JournalEntity.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    public Dao<HabitEntity, Integer> getHabitDao() throws SQLException {
        if (habitDao == null) {
            habitDao = getDao(HabitEntity.class);
        }
        return habitDao;
    }

    public RuntimeExceptionDao<HabitEntity, Integer> getHabitRuntimeDao() {
        if (habitRuntimeDao == null) {
            habitRuntimeDao = getRuntimeExceptionDao(HabitEntity.class);
        }
        return habitRuntimeDao;
    }

    public Dao<HabitToWeekdaysEntity, Integer> getHabitToWeekdayDao() throws SQLException {
        if (habitToWeekdayDao == null) {
            habitToWeekdayDao = getDao(HabitToWeekdaysEntity.class);
        }
        return habitToWeekdayDao;
    }

    public RuntimeExceptionDao<HabitToWeekdaysEntity, Integer> getHabitToWeekdayRuntimeDao() {
        if (habitToWeekdayRuntimeDao == null) {
            habitToWeekdayRuntimeDao = getRuntimeExceptionDao(HabitToWeekdaysEntity.class);
        }
        return habitToWeekdayRuntimeDao;
    }

    public Dao<JournalEntity, Integer> getJournalDao() throws SQLException {
        if (journalDao == null) {
            journalDao = getDao(JournalEntity.class);
        }
        return journalDao;
    }

    public RuntimeExceptionDao<JournalEntity, Integer> getJournalRuntimeDao() {
        if (journalRuntimeDao == null) {
            journalRuntimeDao = getRuntimeExceptionDao(JournalEntity.class);
        }
        return journalRuntimeDao;
    }

    public Dao<ReminderEntity, Integer> getReminderDao() throws SQLException {
        if (reminderDao == null) {
            reminderDao = getDao(ReminderEntity.class);
        }
        return reminderDao;
    }

    public RuntimeExceptionDao<ReminderEntity, Integer> getReminderRuntimeDao() {
        if (reminderRuntimeDao == null) {
            reminderRuntimeDao = getRuntimeExceptionDao(ReminderEntity.class);
        }
        return reminderRuntimeDao;
    }

}

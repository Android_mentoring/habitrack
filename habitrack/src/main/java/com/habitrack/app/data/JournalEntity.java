package com.habitrack.app.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable
public class JournalEntity {
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign = true, columnDefinition = "integer references habit(id) on delete cascade")
    private HabitEntity habit;

    @DatabaseField
    private Date checkedDate;

    @DatabaseField
    private String comment;

    @DatabaseField
    private HabitStatus status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HabitEntity getHabit() {
        return habit;
    }

    public void setHabit(HabitEntity habit) {
        this.habit = habit;
    }

    public Date getCheckedDate() {
        return checkedDate;
    }

    public void setCheckedDate(Date checkedDate) {
        this.checkedDate = checkedDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public HabitStatus getStatus() {
        return status;
    }

    public void setStatus(HabitStatus status) {
        this.status = status;
    }

    public JournalEntity() {
    }
}

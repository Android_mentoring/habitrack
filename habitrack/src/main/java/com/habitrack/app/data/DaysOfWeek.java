package com.habitrack.app.data;

public enum DaysOfWeek {
    MON ("Monday","MON"),
    TUE ("Tuesday","TUE"),
    WED ("Wednesday","WED"),
    THU ("Thursday","THU"),
    FRI ("Friday","FRI"),
    SAT ("Saturday","SAT"),
    SUN ("Sunday","SUN");

    private String value;
    private String shortValue;

    DaysOfWeek(String value, String shortValue) {
        this.value = value;
        this.shortValue = shortValue;
    }

    public String getValue() {
        return value;
    }

    public String getShortValue() {
        return shortValue;
    }

}


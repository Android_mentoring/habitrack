package com.habitrack.app.data.helper;


import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TimePicker;

import com.habitrack.app.R;
import com.habitrack.app.data.DaysOfWeek;
import com.habitrack.app.data.HabitEntity;
import com.habitrack.app.data.HabitToWeekdaysEntity;
import com.habitrack.app.data.HabitType;
import com.habitrack.app.data.ReminderEntity;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;


public class DataManagerImpl implements DataManager {

    private Context ctx;
    private DatabaseHelper databaseHelper = null;

    public DataManagerImpl(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public HabitEntity saveHabit(final View dialogView) {
        final HabitEntity habit = createHabitFromView(dialogView);
        try {
            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(),
                    new Callable<Void>() {
                        public Void call() throws Exception {
                            RuntimeExceptionDao<HabitEntity, Integer> habitDao = getHelper().getHabitRuntimeDao();
                            habitDao.create(habit);
                            addAnotherEntities(dialogView, habit);
                            return null;
                        }
                    });
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return habit;
    }

    @Override
    public List<HabitEntity> getAllHabits() {
        RuntimeExceptionDao<HabitEntity, Integer> habitDao = getHelper().getHabitRuntimeDao();
        return habitDao.queryForAll();
    }

    @Override
    public HabitEntity getHabit(String habitName) {
        RuntimeExceptionDao<HabitEntity, Integer> habitRuntimeDao = getHelper().getHabitRuntimeDao();
        QueryBuilder<HabitEntity, Integer> queryBuilder =
                habitRuntimeDao.queryBuilder();
        PreparedQuery<HabitEntity> preparedQuery = null;

        try {
            queryBuilder.where().eq(HabitEntity.NAME, habitName);
            preparedQuery = queryBuilder.prepare();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return habitRuntimeDao.queryForFirst(preparedQuery);
    }

    @Override
    public void removeHabit(String habitName) {
        RuntimeExceptionDao<HabitEntity, Integer> habitRuntimeDao = getHelper().getHabitRuntimeDao();
        DeleteBuilder<HabitEntity, Integer> deleteBuilder =
                habitRuntimeDao.deleteBuilder();
        try {
            deleteBuilder.where().eq(HabitEntity.NAME, habitName);
            deleteBuilder.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private HabitEntity createHabitFromView(View dialogView) {
        EditText habitName = (EditText) dialogView.findViewById(R.id.habit_name);
        HabitEntity habitEntity = new HabitEntity();
        habitEntity.setName(habitName.getText().toString());
        habitEntity.setType(getHabitType(dialogView));
        CheckBox reminder = (CheckBox) dialogView.findViewById(R.id.reminder_checkbox);
        CheckBox specialReminder = (CheckBox) dialogView.findViewById(R.id.special_reminder_checkbox);
        habitEntity.setReminder(reminder.isChecked());
        habitEntity.setSpecialReminder(specialReminder.isChecked());
        return habitEntity;
    }

    private HabitType getHabitType(View dialogView) {
        RadioGroup habitType = (RadioGroup) dialogView.findViewById(R.id.habit_type);
        int checked = habitType.getCheckedRadioButtonId();
        switch (checked) {
            case R.id.negative_type: return HabitType.NEGATIVE;
            case R.id.neutral_type: return HabitType.NEUTRAL;
            default: return HabitType.POSITIVE;
        }
    }

    private void addAnotherEntities(View dialogView, HabitEntity habit) {
        CheckBox reminder = (CheckBox) dialogView.findViewById(R.id.reminder_checkbox);
        CheckBox specialReminder = (CheckBox) dialogView.findViewById(R.id.special_reminder_checkbox);
        if (reminder.isChecked()) {
            if (specialReminder.isChecked()) {
                saveSpecialReminders(dialogView, habit);
            } else {
                saveReminder(dialogView,habit);
            }
        }

        saveWeekdayEntities(dialogView, habit);

    }

    private void saveWeekdayEntities(View dialogView, HabitEntity habit) {
        prepareAndSaveWeekdayEntity(dialogView, R.id.mon_checkbox,habit);
        prepareAndSaveWeekdayEntity(dialogView, R.id.tue_checkbox,habit);
        prepareAndSaveWeekdayEntity(dialogView, R.id.wed_checkbox,habit);
        prepareAndSaveWeekdayEntity(dialogView, R.id.thu_checkbox,habit);
        prepareAndSaveWeekdayEntity(dialogView, R.id.fri_checkbox,habit);
        prepareAndSaveWeekdayEntity(dialogView, R.id.sat_checkbox,habit);
        prepareAndSaveWeekdayEntity(dialogView, R.id.sun_checkbox,habit);
    }

    private void saveSpecialReminders(View dialogView, HabitEntity habit) {
        prepareAndSaveSpecialReminderEntity(dialogView, R.id.mon_time_picker, R.id.mon_checkbox, habit, DaysOfWeek.MON);
        prepareAndSaveSpecialReminderEntity(dialogView, R.id.tue_time_picker, R.id.tue_checkbox, habit, DaysOfWeek.TUE);
        prepareAndSaveSpecialReminderEntity(dialogView, R.id.wed_time_picker, R.id.wed_checkbox, habit, DaysOfWeek.WED);
        prepareAndSaveSpecialReminderEntity(dialogView, R.id.thu_time_picker, R.id.thu_checkbox, habit, DaysOfWeek.THU);
        prepareAndSaveSpecialReminderEntity(dialogView, R.id.fri_time_picker, R.id.fri_checkbox, habit, DaysOfWeek.FRI);
        prepareAndSaveSpecialReminderEntity(dialogView, R.id.sat_time_picker, R.id.sat_checkbox, habit, DaysOfWeek.SAT);
        prepareAndSaveSpecialReminderEntity(dialogView, R.id.sun_time_picker, R.id.sun_checkbox, habit, DaysOfWeek.SUN);

    }

    private void prepareAndSaveSpecialReminderEntity(View dialogView, int timepickerId, int dayId, HabitEntity habit, DaysOfWeek day) {
        CheckBox dayCheckbox = (CheckBox) dialogView.findViewById(dayId);
        if (dayCheckbox.isChecked()) {
            ReminderEntity reminderEntity = new ReminderEntity();
            reminderEntity.setHabit(habit);
            TimePicker reminder = (TimePicker) dialogView.findViewById(timepickerId);
            reminderEntity.setDay(day);
            reminderEntity.setStartDate((reminder.getCurrentMinute() * 60 + reminder.getCurrentHour() * 60 * 60) * 1000L);
            RuntimeExceptionDao<ReminderEntity, Integer> reminderDao = getHelper().getReminderRuntimeDao();
            reminderDao.create(reminderEntity);
        }
    }


    private void prepareAndSaveWeekdayEntity (View dialogView, int id, HabitEntity habit) {
        CheckBox dayCheckbox = (CheckBox) dialogView.findViewById(id);
        if (dayCheckbox.isChecked()) {
            DaysOfWeek day = DaysOfWeek.valueOf(dayCheckbox.getText().toString().toUpperCase());
            saveWeekday(day,habit);
        }
    }

    private void saveWeekday(DaysOfWeek day, HabitEntity habit) {
            HabitToWeekdaysEntity weekdaysEntity = new HabitToWeekdaysEntity();
            weekdaysEntity.setHabit(habit);
            weekdaysEntity.setDay(day);
            RuntimeExceptionDao<HabitToWeekdaysEntity, Integer> weekdayRuntimeDao = getHelper().getHabitToWeekdayRuntimeDao();
            weekdayRuntimeDao.create(weekdaysEntity);
    }

    private void saveReminder(View dialogView, HabitEntity habit) {
        ReminderEntity reminderEntity = new ReminderEntity();
        reminderEntity.setHabit(habit);
        TimePicker reminder = (TimePicker) dialogView.findViewById(R.id.start_reminder_time_picker);
        reminderEntity.setStartDate((reminder.getCurrentMinute() * 60 + reminder.getCurrentHour() * 60 * 60) * 1000L);
        RuntimeExceptionDao<ReminderEntity, Integer> reminderDao = getHelper().getReminderRuntimeDao();
        reminderDao.create(reminderEntity);
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper =
                    OpenHelperManager.getHelper(ctx, DatabaseHelper.class);
        }
        return databaseHelper;
    }
}

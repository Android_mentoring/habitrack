package com.habitrack.app.data.helper;

import android.view.View;

import com.habitrack.app.data.HabitEntity;

import java.util.List;

public interface DataManager {

    public HabitEntity saveHabit(View dialogView);
    public List<HabitEntity> getAllHabits();
    public HabitEntity getHabit(String habitName);
    public void removeHabit(String habitName);
}

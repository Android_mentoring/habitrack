package com.habitrack.app.data;


import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;


//todo maybe add orderId
@DatabaseTable
public class HabitEntity {

    public static final String NAME = "name";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = NAME)
    private String name;

    @DatabaseField
    private HabitType type;

    @DatabaseField
    private boolean specialReminder;

    @DatabaseField
    private boolean reminder;


    @ForeignCollectionField
    private ForeignCollection<ReminderEntity> reminders;

    @ForeignCollectionField
    private ForeignCollection<HabitToWeekdaysEntity> weekdays;

    @ForeignCollectionField
    private ForeignCollection<JournalEntity> journalEntities;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HabitType getType() {
        return type;
    }

    public void setType(HabitType type) {
        this.type = type;
    }

    public boolean isSpecialReminder() {
        return specialReminder;
    }

    public void setSpecialReminder(boolean specialReminder) {
        this.specialReminder = specialReminder;
    }

    public boolean isReminder() {
        return reminder;
    }

    public void setReminder(boolean reminder) {
        this.reminder = reminder;
    }

    public ForeignCollection<ReminderEntity> getReminders() {
        return reminders;
    }

    public void setReminders(ForeignCollection<ReminderEntity> reminders) {
        this.reminders = reminders;
    }

    public ForeignCollection<HabitToWeekdaysEntity> getWeekdays() {
        return weekdays;
    }

    public void setWeekdays(ForeignCollection<HabitToWeekdaysEntity> weekdays) {
        this.weekdays = weekdays;
    }

    public ForeignCollection<JournalEntity> getJournalEntities() {
        return journalEntities;
    }

    public void setJournalEntities(ForeignCollection<JournalEntity> journalEntities) {
        this.journalEntities = journalEntities;
    }

    public HabitEntity() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HabitEntity that = (HabitEntity) o;

        if (id != that.id) return false;
        if (reminder != that.reminder) return false;
        if (specialReminder != that.specialReminder) return false;
        if (!name.equals(that.name)) return false;
        if (type != that.type) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + (specialReminder ? 1 : 0);
        result = 31 * result + (reminder ? 1 : 0);
        return result;
    }
}

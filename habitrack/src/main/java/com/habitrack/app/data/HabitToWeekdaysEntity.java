package com.habitrack.app.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class HabitToWeekdaysEntity {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign = true, columnDefinition = "integer references habit(id) on delete cascade")
    private HabitEntity habit;

    @DatabaseField
    private DaysOfWeek day;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HabitEntity getHabit() {
        return habit;
    }

    public void setHabit(HabitEntity habit) {
        this.habit = habit;
    }

    public DaysOfWeek getDay() {
        return day;
    }

    public void setDay(DaysOfWeek day) {
        this.day = day;
    }

    public HabitToWeekdaysEntity() {
    }
}

package com.habitrack.app.application;


import android.app.Application;
import android.view.View;

import com.habitrack.app.data.HabitEntity;
import com.habitrack.app.data.helper.DataManager;
import com.habitrack.app.data.helper.DataManagerImpl;

import java.util.ArrayList;
import java.util.List;

public class HabitApplication extends Application {
    private DataManager dataManager;
    private List<HabitEntity> habits = new ArrayList<HabitEntity>();

    @Override
    public void onCreate() {
        super.onCreate();
        dataManager = new DataManagerImpl(getApplicationContext());
        fillData();
    }

    public void saveHabit(View dialogView) {
        habits.add(dataManager.saveHabit(dialogView));
    }

    public List<HabitEntity> getHabits() {
        return habits;
    }

    public void replaceHabit(HabitEntity replacedHabit, View dialogView) {
        int index = habits.indexOf(replacedHabit);
        dataManager.removeHabit(replacedHabit.getName());
        habits.set(index, dataManager.saveHabit(dialogView));
    }

    protected void fillData() {
        habits = dataManager.getAllHabits();
    }

    public HabitEntity getHabit(String habitName) {
        return dataManager.getHabit(habitName);
    }
}

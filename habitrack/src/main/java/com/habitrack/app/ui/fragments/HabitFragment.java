package com.habitrack.app.ui.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.habitrack.app.R;
import com.habitrack.app.application.HabitApplication;
import com.habitrack.app.data.HabitEntity;
import com.habitrack.app.services.HabitAdapter;

import java.util.HashMap;
import java.util.List;

public class HabitFragment extends Fragment {

    List<HabitEntity> habits = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fillData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_habit, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ListView habitListView = (ListView) getActivity().findViewById(R.id.habit_list);
        final HabitAdapter habitAdapter = new HabitAdapter(getActivity().getActionBar().getThemedContext(), habits);
        habitListView.setAdapter(habitAdapter);
        habitListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                HabitEntity habitEntity = (HabitEntity) habitAdapter.getItem(position);
                FragmentManager fragmentManager = getFragmentManager();
                EditOrNewHabitDialog newFragment = new EditOrNewHabitDialog(true, habitEntity.getName());
                newFragment.show(fragmentManager, "dialog");
                return true;
            }
        });
    }

    void fillData() {
        HabitApplication app = (HabitApplication) getActivity().getApplication();
        habits = app.getHabits();
    }
}

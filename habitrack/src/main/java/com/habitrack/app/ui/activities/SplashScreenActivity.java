package com.habitrack.app.ui.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;

import com.habitrack.app.R;


public class SplashScreenActivity extends Activity {

    private static int SPLASH_TIME_OUT = 3000;
    private boolean onTouched;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
                if (!onTouched) {
                    Intent i = new Intent(SplashScreenActivity.this, HomeActivity.class);
                    startActivity(i);
                }
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        onTouched = true;
        Intent i = new Intent(SplashScreenActivity.this, HomeActivity.class);
        startActivity(i);
        return true;
    }
}
package com.habitrack.app.ui.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.habitrack.app.R;
import com.habitrack.app.application.HabitApplication;
import com.habitrack.app.data.HabitEntity;
import com.habitrack.app.services.EditOrNewHabitInitialisationHelper;
import com.habitrack.app.services.HabitAdapter;

public class EditOrNewHabitDialog extends DialogFragment {

    EditOrNewHabitInitialisationHelper initService = new EditOrNewHabitInitialisationHelper();
    private final String habitName;

    private final boolean isEdit;

    public EditOrNewHabitDialog(boolean isEdit, String habitName) {
        super();
        this.isEdit = isEdit;
        this.habitName = habitName;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View dialogView = getActivity().getLayoutInflater().inflate(R.layout.add_new_dialog, null);

        String title = getString(R.string.title_new_habit);;

        HabitEntity habitEntity = null;
        if (isEdit) {
            title = getString(R.string.title_edit_habit);
            HabitApplication app = (HabitApplication) getActivity().getApplication();
            habitEntity = app.getHabit(habitName);
            initService.initReminders(dialogView, habitEntity);
            initService.initHabitName(dialogView,habitName);
            initService.initHabitType(dialogView, habitEntity);
        }

        final HabitEntity finalHabitEntity = habitEntity;
        builder.setTitle(title).setView(dialogView)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        HabitApplication app = (HabitApplication) getActivity().getApplication();
                        if (isEdit) {
                            app.replaceHabit(finalHabitEntity, dialogView);
                        } else {
                            app.saveHabit(dialogView);
                        }
                        notifyAdapter();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        initService.initReminderCheckboxes(dialogView, habitEntity);
        initService.initTabs(dialogView);
        initService.initDaysCheckboxes(dialogView, habitEntity);
        return builder.create();
    }

    private void notifyAdapter() {
        ListView habitListView = (ListView) getActivity().findViewById(R.id.habit_list);
        ((HabitAdapter) habitListView.getAdapter()).notifyDataSetChanged();
    }


}

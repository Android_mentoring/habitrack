package com.habitrack.app.ui.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;

import com.habitrack.app.R;
import com.habitrack.app.ui.fragments.EditOrNewHabitDialog;
import com.habitrack.app.ui.fragments.HabitFragment;
import com.habitrack.app.ui.fragments.NavigationDrawerFragment;
import com.habitrack.app.ui.fragments.NotesFragment;
import com.habitrack.app.ui.fragments.StatisticFragment;

public class HomeActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        mTitle = getString(R.string.habit_section);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getFragmentManager();
        switch (position) {
            case 0:
                mTitle = getString(R.string.habit_section);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, new HabitFragment())
                        .commit();
                break;
            case 1:
                mTitle = getString(R.string.statistic_section);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, new StatisticFragment())
                        .commit();
                break;
            case 2:
                mTitle = getString(R.string.notes_section);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, new NotesFragment())
                        .commit();
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_HOME_AS_UP);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setCustomView(R.layout.actionbar);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_add) {
            FragmentManager fragmentManager = getFragmentManager();
            EditOrNewHabitDialog newFragment = new EditOrNewHabitDialog(false, null);
            newFragment.show(fragmentManager, "dialog");
            return true;
        }
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }
}

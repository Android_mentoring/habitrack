package com.habitrack.app.handlers;

import android.graphics.Color;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TabHost;
import android.widget.TextView;

public class WeekDayCheckboxHandler implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private int tabNumber;
    private CheckBox checkbox;
    private TabHost tabHost;

    public WeekDayCheckboxHandler(int tabNumber, CheckBox checkbox, TabHost tabhost) {
        this.tabNumber = tabNumber;
        this.checkbox = checkbox;
        this.tabHost = tabhost;
    }

    @Override
    public void onClick(View v) {
        setTabState();
    }

    @SuppressWarnings("ConstantConditions")
    private void setTabState() {
        if (!checkbox.isChecked()) {
            tabHost.getTabWidget().getChildTabViewAt(tabNumber).setEnabled(false);
            TextView tv = (TextView) tabHost.getTabWidget().getChildAt(tabNumber).findViewById(android.R.id.title);
            // todo
            tv.setTextColor(Color.parseColor("#ffffff"));
            if (tabHost.getCurrentTab() == tabNumber) {
                tabHost.setCurrentTab(getNearestEnabledTab(tabNumber,tabHost));
            }
        } else {
            tabHost.getTabWidget().getChildTabViewAt(tabNumber).setEnabled(true);
            TextView tv = (TextView) tabHost.getTabWidget().getChildAt(tabNumber).findViewById(android.R.id.title);
            tv.setTextColor(Color.parseColor("#000000"));
        }
    }

    @SuppressWarnings("ConstantConditions")
    private int getNearestEnabledTab(int tabNumber, TabHost tabhost) {
        for (int i = tabNumber+1; i < 7; i++) {
            if (tabhost.getTabWidget().getChildTabViewAt(i).isEnabled()) {
                return i;
            }
        }
        for (int i = 0; i < tabNumber; i++) {
            if (tabhost.getTabWidget().getChildTabViewAt(i).isEnabled()) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        setTabState();
    }
}
